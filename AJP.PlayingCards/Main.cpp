//Aaron Payne
// Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

// enums for rank & suit

enum Rank {
	Two = 2, Three,	Four, Five,	Six,
	Seven, Eight, Nine,	Ten,
	Jack, Queen, King, Ace
};

enum Suit {
	Hearts, Clubs, Spades, Diamonds
};

// struct for card
struct Card {
	Rank rank;
	Suit suit;
};

int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	if (a.rank < b.rank)
	{
		cout << "Ace of Spades it greater than the Jack of Hearts\n";
	}

	(void)_getch();
	return 0;
}